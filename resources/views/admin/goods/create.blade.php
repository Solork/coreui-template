@extends('admin.layout')

@section('title', 'Admin - Barang/Buat')

@section('breadcrumb')
<ol class="breadcrumb">
    <li class="breadcrumb-item">Admin</li>
    <li class="breadcrumb-item">Dasbor</li>
    <li class="breadcrumb-item active">Barang</li>
</ol>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <h3>New Barang</h3>
                </div>
                <hr>
                @include('admin.shared.components.validation-alert')
                @include('admin.goods.partials.form-create')
            </div>
        </div>
    </div>
@endsection
