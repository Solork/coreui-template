@extends('admin.layout')

@section('title', 'Admin - Barang')

@section('unique-css')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Admin</li>
        <li class="breadcrumb-item">Dasbor</li>
        <li class="breadcrumb-item active">Barang</li>
    </ol>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="card-title">
                            <h3>Tabel Barang</h3>
                        </div>
                    </div>
                    <div class="col">
                        <a href="{{ route('admin.goods.create') }}" class="btn btn-primary float-right">New</a>
                    </div>
                </div>
                <hr>
                @include('admin.shared.components.alert')
                @include('admin.goods.partials.table')
                @include('admin.shared.components.modal-delete')
            </div>
        </div>
    </div>
@endsection

@section('unique-js')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var table = $('#myTable').DataTable()
        });
    </script>
    <script>
        function delete_action(action) {
            $('#btnDelete').data('action', action)
        }

        $('#btnDelete').on('click', function() {
            $('#formDelete').attr('action', $(this).data('action'))
            $('#formDelete').submit()
        })
    </script>
@endsection
