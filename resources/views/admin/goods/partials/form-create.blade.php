<form action="{{ Route('admin.goods.store') }}" method="post">
    @csrf
    <div class="form-group">
        <label for="name">Nama</label>
        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
    </div>
    <div class="form-group">
        <label for="jenis">Jenis</label>
        <select class="form-control" name="goods_type_id" required>
            @foreach ($types as $item)
                <option value="{{ $item->id }}" {{ old('goods_type_id') == $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="code">Kode</label>
        <input type="text" class="form-control" name="code" value="{{ old('code') }}" required>
    </div>
    <div class="form-group">
        <label for="quantity">Jumlah</label>
        <input type="number" class="form-control" name="quantity" value="{{ old('quantity') }}" required min="1">
    </div>
    {{-- <div class="form-group">
        <label for="address">Penyimpanan</label>
        <textarea class="form-control" name="address" cols="30" rows="5">{{ old('address') }}</textarea>
    </div> --}}
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
