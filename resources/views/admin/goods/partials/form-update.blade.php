<form action="{{ Route('admin.goods.update', $goods->id) }}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="name">Nama</label>
        <input type="text" class="form-control" name="name" value="{{ $goods->name }}" required>
    </div>
    <div class="form-group">
        <label for="jenis">Jenis</label>
        <select class="form-control" name="goods_type_id" required>
            @foreach ($types as $item)
                <option value="{{ $item->id }}" {{ $goods->goods_type_id == $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="code">Kode</label>
        <input type="text" class="form-control" name="code" value="{{ $goods->code }}" required>
    </div>
    <div class="form-group">
        <label for="quantity">Jumlah</label>
        <input type="number" class="form-control" name="quantity" value="{{ $goods->quantity }}" required min="1">
    </div>
    <div class="form-group">
        <label for="unavailable">Ketersediaan Barang <small><strong>centang barang yang sedang dipinjam/tidak tersedia</strong></small></label>
        <select multiple name="unavailable[]" class="form-control">
            @for ($i = 1; $i <= $goods->quantity; $i++)
                <option value="{{ $i }}" {{ Arr::exists((array) $goods->unavailable, $i) }}>{{ $i }}</option>
            @endfor
        </select>
    </div>
    {{-- <div class="form-group">
        <label for="address">Penyimpanan</label>
        <textarea class="form-control" name="address" cols="30" rows="5">{{ $goods->address }}</textarea>
    </div> --}}
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
