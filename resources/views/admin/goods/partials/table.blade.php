<table id="myTable" class="table table-responsive-sm table-striped">
    <thead>
        <th>#</th>
        <th>Nama</th>
        <th>Kode</th>
        <th>Jenis</th>
        <th>Jumlah</th>
        <th>Sisa</th>
        <th>Aksi</th>
    </thead>
    <tbody>
        @php $no=1; @endphp
        @foreach ($goods as $key => $value)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $value->name }}</td>
                <td>{{ $value->code }}</td>
                <td>{{ @$value->goods_type->name ? : "KOSONG" }}</td>
                <td>{{ $value->quantity }}</td>
                <td>{{ $value->sisa }}</td>
                <td style="text-align:center;">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fas fa-list"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="{{ Route('admin.goods.edit', $value->id) }}" class="nav-link">Details/Edit</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#delConfModal" data-action="{{ Route('admin.goods.destroy', $value->id) }}" onclick="delete_action($(this).data('action'))" class="nav-link">Hapus</a></li>
                    </ul>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
