<!DOCTYPE html>
<html lang="en">
	<head>
		@include('admin.shared.partials.meta')
		<title>@yield('title')</title>
		@include('admin.shared.partials.css')
		@yield('unique-css')
	</head>
	<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
		@include('admin.shared.partials.navbar')
		<div class="app-body">
			@include('admin.shared.partials.sidebar')
			<main class="main">
				@yield('breadcrumb')
				@yield('content')
			</main>
		</div>
		@include('admin.shared.partials.footer')
		@include('admin.shared.partials.js')
		@yield('unique-js')
	</body>
</html>
